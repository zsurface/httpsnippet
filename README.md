### Friday, 29 October 2021 16:09 PDT
## Fixed newline issue

``` haskell
	let s = "ab\ncd"
	lines s  -- ["ab", "cd"]
	unlines $ lines s -- "ab\ncd\n"  -- extra a newline '\n'
```

## Thing that I learned

``` haskell
   mapM_(\x -> do
              putStrLn x
	) ["a", "b"]

   mapM_(\x -> do
              putStr x
	) ["a", "b"]
```

### Saturday, 30 October 2021 12:44 PDT
## Fixed fetching unicode char issue 
## Using Network.HTTP.Client instead of Network.HTTP.simpleHTTP

### Broken package in Haskage
* [Network.HTTP.simpleHTTP](https://hackage.haskell.org/package/HTTP-4000.3.16)
* The http client is broken for unicode

``` haskell
    import Network.HTTP  -- Broken package
	let host = "http://localhost:8080/snippet?id=apl+enclose"
    rsptest <- Network.HTTP.simpleHTTP (getRequest host)  -- simpleHTTP is borken for unicode
	pre rsptest
```
* Does not print unicode characters properly.

### Thursday, 04 November 2021 01:55 PDT
## Add color highlight for small numbers of Haskell keywords.
## There is still a bug for '[' and ']' characters because escape sequence ascii color use '[' and ']' too. e.g. '\\x1b[32m'

``` haskell
   let s3 = map (\x -> searchReplaceWord x "awk" "\x1b[32m" ++ "\\0" ++ "\x1b[0m") s2
```
* Use highlight color in Haskell functions.

``` bash
	HttpSnippet -hi k split
```

* [See Color Highlight](http://xfido.com/image/httpsnippet_highlight.png)

### Wednesday, 29 December 2021 14:20 PST
* Add code to query Cpp function ($cpplib/AronLib.h)

``` bash
                 + -> Highlight  
                 ↓
	HttpSnippet -hi x list
                    ↑
					+ -> Cpp AronLib.h
```

* Refactor code to Highlight code using *Better* Haskell function
* Replace a list of *words* in a line
* [Cpp Color Highlight](http://xfido.com/image/httpsnippet_cpp.png)

``` haskell
	let s050 = lines $ searchReplaceListWord repList (unlines s05)
```
