{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_HttpSnippet (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/HttpSnippet/.stack-work/install/x86_64-osx/a5b7ecff00c6fca3ac2dcbd89391d1263425251653fcdb8f9f72f43bf9d48258/8.8.4/bin"
libdir     = "/Users/aaa/myfile/bitbucket/HttpSnippet/.stack-work/install/x86_64-osx/a5b7ecff00c6fca3ac2dcbd89391d1263425251653fcdb8f9f72f43bf9d48258/8.8.4/lib/x86_64-osx-ghc-8.8.4/HttpSnippet-0.1.0.0-Fve6tUFvYUrK3RjeZxYj1P-HttpSnippet"
dynlibdir  = "/Users/aaa/myfile/bitbucket/HttpSnippet/.stack-work/install/x86_64-osx/a5b7ecff00c6fca3ac2dcbd89391d1263425251653fcdb8f9f72f43bf9d48258/8.8.4/lib/x86_64-osx-ghc-8.8.4"
datadir    = "/Users/aaa/myfile/bitbucket/HttpSnippet/.stack-work/install/x86_64-osx/a5b7ecff00c6fca3ac2dcbd89391d1263425251653fcdb8f9f72f43bf9d48258/8.8.4/share/x86_64-osx-ghc-8.8.4/HttpSnippet-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/HttpSnippet/.stack-work/install/x86_64-osx/a5b7ecff00c6fca3ac2dcbd89391d1263425251653fcdb8f9f72f43bf9d48258/8.8.4/libexec/x86_64-osx-ghc-8.8.4/HttpSnippet-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/HttpSnippet/.stack-work/install/x86_64-osx/a5b7ecff00c6fca3ac2dcbd89391d1263425251653fcdb8f9f72f43bf9d48258/8.8.4/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "HttpSnippet_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "HttpSnippet_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "HttpSnippet_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "HttpSnippet_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "HttpSnippet_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "HttpSnippet_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
