-- {{{ begin_fold
-- script
{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"
{-# LANGUAGE MultiWayIf #-}

-- import Data.Set   -- collide with Data.List 
import Data.Char()
import qualified Data.List as L
import Data.List.Split()
import Data.Time()
import Data.Time.Clock.POSIX()
import Data.Typeable
import System.Directory()
import System.Environment
import System.Exit()
import System.FilePath.Posix()
import System.Posix.Files()
import System.Posix.Unistd()
import System.Process()
import Text.Read()
import Text.Regex()
import Text.Regex.Base()
import Text.Regex.Base.RegexLike()
import Text.Regex.Posix()
import Data.IORef() 
import Control.Monad()
import Control.Concurrent() 

-- import Network.HTTP  -- Broken package

import qualified Network.HTTP.Simple as HTTPSIMPLE
import qualified Network.HTTP2.Client as HTC
import Network.URI.Encode()
import Rainbow()
import System.Console.Pretty (Color (..), color)

-- https://hackage.haskell.org/package/ansi-terminal-0.10.3/docs/System-Console-ANSI.html
import System.IO
import qualified System.Console.ANSI as AN
       
import AronModule hiding(rw)

-- KEY: Get data from Wai Http Server, query snippet from httpd, console color, ansi color
-- The code is use vimscript to call Wail Server through 8080
-- /Users/cat/myfile/bitbucket/vim/vim.vimrc:274
-- 
-- Server: /Users/cat/myfile/bitbucket/haskell_webapp/wai.hs
--
-- Wed May  8 15:52:21 2019 
-- Snippet:         "s regex" => Snippet
-- Aron.java:       "j list" => Java Aron.*
-- Print.java:      "p pr"   => Java Print.*
-- AronModule.hs    "h list" => Haskell
--    var | var == "x " -> responseXCmd s         -- forget what it is for.
--        | var == "c " -> responseCmd conn s     -- Shell commands
--        | var == "j " -> responseJavaHtml s     -- Java AronLib.java with Html, css.
--        | var == "h " -> responseHaskellHtml s  -- Haskell AronModule.hs with Html, css.
--        | var == "k " -> queryLibHaskell s    -- Haskell AronModule.hs
--        | var == "i " -> queryLibJava s       -- Java $b/javalib/AronLib.java
--        | var == "p " -> queryLibJavaPackage "Print." s -- Java $b/javalib/Print.java
--        | var == "n " -> responseSnippetTxt s ref  -- Snippet with NO Html, css.
--        | otherwise   -> responseSnippetHtml conn s ref  -- Snippet with Html, css.
--
-- Sun May 12 11:58:33 2019  
-- Remove code from Html to Text
-- return Text file from Wai server directly
-------------------------------------------------------------------------------- 


{-| 
    * Optional argument
    * `NoHead` => "-nh" => no header
    * `Color`  => "-co" => escape sequence ascii color 
    * `High`   => "-hi" => Highlight word 
-} 
data Option = NoHead | Color  | High | Page

opt::Option -> String 
opt  NoHead = "-nh"
opt  Color  = "-co"
opt  High   = "-hi"
opt  Page   = "-p"

esc_SEQ="\x1b["                   
fg_BLACK=       esc_SEQ   ++ "30m"
fg_RED=         esc_SEQ   ++ "31m"
fg_GREEN=       esc_SEQ   ++ "32m"
fg_YELLOW=      esc_SEQ   ++ "33m"
fg_BLUE=        esc_SEQ   ++ "34m"
fg_MAGENTA=     esc_SEQ   ++ "35m"
fg_CYAN=        esc_SEQ   ++ "36m"
fg_WHITE=       esc_SEQ   ++ "37m"
fg_BR_BLACK=    esc_SEQ   ++ "90m"
fg_BR_RED=      esc_SEQ   ++ "91m"
fg_BR_GREEN=    esc_SEQ   ++ "92m"
fg_BR_YELLOW=   esc_SEQ   ++ "93m"
fg_BR_BLUE=     esc_SEQ   ++ "94m"
fg_BR_MAGENTA=  esc_SEQ   ++ "95m"
fg_BR_CYAN=     esc_SEQ   ++ "96m"
fg_BR_WHITE=    esc_SEQ   ++ "97m"
                                  
fg_END=         "\x1b[0m"


-- url = "http://127.0.0.1:8080/snippet?id=s+emacs"
-- NOTE: http://localhost:8080  DOES NOT WORK
url = "http://127.0.0.1:8080"
-- url = "http://xfido.com:8081/snippet?id="
  
{-|
   DATE: Friday, 29 October 2021 16:04 PDT
   FIXED: newline issue when codeblock is outputed to console
-}

{-| 
    === line with color
    psTab 5 (color Green   (replicate 80 '-'))
-} 
lineColor::IO()
lineColor = psTab 5 (color Green   (replicate 80 '-'))

{-| 
    * Make http call with following options,

    [Default option] include header and no color

    [-nh] no header
    [-co] escape ascii color


    >-nh  => no header
    >-co  => no escape ascii(console) color
    >
    >
    > HttpSnippet s java

    *  no option

    >
    > HttpSnippet -nh s java

    * header is removed, only code block is left 

    @
    & HttpSnippet -nh s java

    java_regex:*: java
    line 1
    line 2

    output:
    line 1
    line 2
    @

    
    * HttpSnippet -nh -co s java
    * no header with color 

    >
    > HttpSnippet -co s java

    * [block code] includes escape ascii(console) color

    @
    & HttpSnippet -co s java

    java_regex:*: java
    line 1
    line 2

    output: 
    java_regex:*: java
    [ascii] line 1 [ascii]
    [ascii] line 2 [ascii]
    @

    * HttpSnippet -co -nh  s java
    * opList = ["-co", "-nh", "-hi"]
    * -nh  => no header
    * -co  => no escape ascii (console) color
    * -hi  => highlight word 
    * param = "s+java"
-}

{-|
    === make a host from different URL such as localhost or xfido.com

    url = "http://xfido.com:8081/snippet?id="
-}
mkHost::String -> String -> String
mkHost url param = url + "/snippet?id=" + param
  where
    (+) = (++)

getIndex::(Ord a) => a -> [a] -> Integer
getIndex a cx = let l = len $ takeWhile (/= a) cx in if l < len cx then l else -1

fws::String -> String
fws s = let h = fi $ div (80 - len s) 2; dash = replicate h '-' in dash ++ s ++ dash
  

main = do

        line <- getArgs 
        if (not . null) line then do
            let lbs2str = lazyTextToStr . lazyByteStringToLazyText 
            let le = len line
            let inx = getIndex "-u" line
            -- specify host name else using url
            -- -u http://xfido.com 
            let host    = if inx >= 0 && inx < (le - 1) then line !! fi (inx + 1) else url
            let lineArg = if inx >= 0 && inx < (le - 1) then removeIndex (integerToInt inx) $ removeIndex (integerToInt inx) line  else line
            let opList  = takeWhile(\x -> head x == '-') lineArg
            let param   = concatStr (dropWhile(\x -> head x == '-') lineArg) "+"  -- param = "s+java"
            -- let param = init $ foldr (\x y -> x ++ "+" ++ y) [] line
            -- putStrLn param
            -- let req = mkHost host param
            -- pp $ "[" <<< req <<< "]"
            -- TODO: get text without any Html
            
            -- rsptest <- Network.HTTP.simpleHTTP (getRequest $ mkHost host param)  -- simpleHTTP is borken for unicode
            req <- HTTPSIMPLE.parseRequest $ mkHost host param
            rsp  <- HTTPSIMPLE.httpLBS req
            
            -- fetch document and return it (as a 'String'.)
            -- s <- fmap (take 100000) (getResponseBody rsp)
            let s = lbs2str $ HTTPSIMPLE.getResponseBody rsp

            -- foldr f z cx =>  (1 `f` (2  `f` (3 `f` z)))
            -- 
            -- remove the top or head line
            let repWord = searchReplaceWord  -- word only
            let repAny  = searchReplaceAnyTup  -- any String
            let s0 = lines s
            -- \9056 = \x2360 => ⍠
            let s01 = map (\x -> repAny x ("\\[",   (color Red "-LLLL-"))) s0
            -- let s01 = map (\x -> repAny x ("\\[",   (color Red "LLLL"))) s0
            -- let s02 = map (\x -> repAny x ("\\]",   (color Red "RRRR"))) s01
            let s02 = map (\x -> repAny x ("\\]",   (color Red "-RRRR-"))) s01
            let s1 = map (\x -> repWord x ("sed",   (color Red "\\0"))   ) s02
            let s2 = map (\x -> repWord x ("grep",  (color White "\\0")) ) s1 
            let s3 = map (\x -> repWord x ("awk",   (color Red "\\0"))   ) s2 
            let s4 = map (\x -> repAny x  ("->",    (color White "\\0")) ) s3
            let s5 = map (\x -> repAny x  ("=>",    (color Red "\\0"))   ) s4
            let s6 = map (\x -> repAny x  ("::",    (color Red " \\0 ")) ) s5
            -- let s7 = map (\x -> searchReplaceAny x "Int" (color Green "\\0") ) s6
            let s7 = map (\x -> repWord x ("Int",   (color Green "\\0")) ) s6
            let s8 = map (\x -> repWord x ("Integer",   (color Green "\\0")) ) s7
            let s9 = map (\x -> repWord x ("String",   (color Green "\\0")) ) s8
            let s10 = map (\x -> repWord x ("Char",   (color Green "\\0")) ) s9
            let s11 = map (\x -> repWord x ("Bool",   (color Green "\\0")) ) s10
            
            let s12 = map (\x -> repAny x ("\\(",   (color Cyan "\\0")) ) s11
            let s13 = map (\x -> repAny x ("\\)",   (color Cyan "\\0")) ) s12
            -- let s10 = map (\x -> repAny x ("LLLL",   (color Red "["))  ) s9
            -- let s11 = map (\x -> repAny x ("RRRR",   (color Red "]"))  ) s10
            let s14 = map (\x -> repAny x ("-LLLL-",   (color Red "["))  ) s13
            let s15 = map (\x -> repAny x ("-RRRR-",   (color Red "]"))  ) s14
            -- let s12 = map (\x -> repAny x ("LLLL",   (color Red "\\0"))  ) s11
            -- let s13 = map (\x -> repAny x ("RRRR",   (color Red "\\0"))  ) s12


--            let s3 = map (\x -> searchReplace x "awk" "\x1b[32m" ++ "\\0" ++ "\x1b[0m") s2
            -- let ls = map(\x -> if len x > 0 then unlines $ (if (opt NoHead) `elem` opList then tail x  else x) else "" ) $ splitListEmptyLine $ lines s
            
            -- color is NOT working so far.. 
            -- let diffColorList = map(\x -> unlines $ map(\(n, b) -> if div n 2 == 0 then color Red b else color Pink b ) (zip [0..] x) ) $ splitListEmptyLine $ lines s
            -- let diffColorList = map(\x -> unlines $ map(\(n, b) -> color Red b) (zip [0..] x) ) $ splitListEmptyLine $ lines s
            let diffColorList = map (unlines . zipWith (curry snd) [0..]) $ splitListEmptyLine $ lines s
            -- pre diffColorList

            -- let lsm = if (opt Color) `elem` opList then lss else ls 
            let outList = if | opt NoHead `elem` opList -> map(\x -> if len x > 0 then unlines $ tail x else "") $ splitListEmptyLine s15
                             | opt High   `elem` opList -> map(\x -> if len x > 0 then unlines x else "") $ splitListEmptyLine s15
                             | opt Color  `elem` opList -> diffColorList 
                             | otherwise                -> map(\x -> if len x > 0 then unlines x else "" ) $ splitListEmptyLine $ lines s

            -- mapM_ (\x -> putStrLn x) lsm
            -- pre outList
            let zoutList = zip [1..] outList
            mapM_ (\(n, x) -> do
                              -- show code in page style
                              if opt Page `elem` opList then do
                                clear
                                setCursorPos 2 5
                                mapM_ (psTab 5) $ lines x   -- lines x => [String]
                                tmp <- getEnv "t"
                                writeFile tmp x
                                setCursorPos (2 + len (lines x) + 1) 5
                                psTab 5 $ "[" ++ show (len outList) ++ "] => " ++ show n
                                setCursorPos (2 + len (lines x) + 1) 20 
                                psTab 5 $ "Code => [" ++ tmp ++ "]"
                                bo <- AN.hSupportsANSI stdout 
                                if bo then print "YES" else print "NO"
                                size <- getTerminalSize
                                psTab 5 $ "size=" ++ show size
                                _ <- getLine
                                clear
                                setCursorPos 2 5
                                psTab 5 (color Red $ replicate 80 '-')
                              else do
                              -- show all code at once (no page style)
                                putStrLn x
                         ) zoutList  -- outList => [String] 
            -- putStr $ unlines ls
            -- writeFile "/tmp/xx.html" s
            -- html to text file
            -- text <- run "w3m -dump /tmp/xx.html"
            -- pp text
            -- mapM putStrLn text 
            -- putStr "END" 
        else do
            psTab 5 (color Red $ fws " No Html Output ")
            psTab 5 (color Red "HttpSnippet n emacs => snippet => snippet.hs")
            psTab 5 (color Red "HttpSnippet k list  => Haskell => AronModule.hs")
            psTab 5 (color Red "HttpSnippet i list  => Java    => Aron.java")
            psTab 5 (color Red "HttpSnippet p pr    => Java    => Print.java")
            psTab 5 (color White $ replicate 80 '-')
            psTab 5 (color White "HttpSnippet -p n latex => pager")
            psTab 5 (color White "HttpSnippet -co n latex => color")
            psTab 5 (color White "HttpSnippet -hi n latex => highlight")
            lineColor
            psTab 5 (color Yellow $ fws " Html Output ")
            psTab 5 (color Yellow "HttpSnippet j list  => Java    => Aron.java")
            psTab 5 (color Yellow "HttpSnippet s regex => Snippet => snippet.hs")
            psTab 5 (color Yellow "HttpSnippet h list  => Haskell => AronModule.hs")
            lineColor
            psTab 5 (color White $ replicate 80 '-')
            psTab 5 (color White "HttpSnippet n emacs  => default http://127.0.0.1:8080")
            psTab 5 (color White "HttpSnippet -u http://127.0.0.1:8080 n emacs")
            psTab 5 (color White "HttpSnippet -u http://xfido:8080 n emacs")
            psTab 5 (color White "HttpSnippet -u localhost:8080 n emacs => DOES NOT WORK")
psTab 5 (color White $ replicate 80 '-')
            psTab 5 (color Green "HttpSnippet DOES NOT connect to Redis.")
            psTab 5 (color Yellow "HttpSnippet => redis_query.hs Aron.java")
            psTab 5 (color Yellow "HttpSnippet => redis_query.hs Print.java")
            psTab 5 (color Yellow "HttpSnippet => redis_query.hs AronModule.hs")
            lineColor
        -- run "haskellbin httpSnippet.hs httpSnippet" 
